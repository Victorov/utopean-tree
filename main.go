package main

import "fmt"

const DEF = 1

func valid(val, min, max int) int {
	if val < min {
		return min
	} else if val > max {
		return max
	}
	return val
}

func main() {
	var n int
	var cycles []int
	fmt.Scanf("%v\n", &n)
	n = valid(n, 1, 10)
	for i := 0; i < n; i++ {
		var val int
		fmt.Scanf("%v\n", &val)
		val = valid(val, 0, 60)
		cycles = append(cycles, val)
	}
	for _, cycle := range cycles {
		height := DEF
		for i := 0; i < cycle; i++ {
			if i%2 == 0 {
				height *= 2
			} else {
				height++
			}

		}
		fmt.Printf("%v\n", height)
	}
}
